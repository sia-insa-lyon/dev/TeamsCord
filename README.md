# TeamsCord

## But du projet

Le but de ce projet est de réaliser une application de messagerie instantannée. Efficiace, simple, jolie et facile à prendre en main.

## Contexte

Après avoir essayé différents outils de comunications, aucun ne nous satifsait pleinement. Sauf Matrix, les protocols et logiciels ne sont pas open source. Et on n'est aps sur que les entreprises de se servent pas des données qu'elles récupèrent. 

<details>
<summary> Liste des points qui nous ont géné dans certaines applications </summary>
#### Discord : 

- Ne peut pas être hébergé. 
- Limite du stream à 50/10 personnes
- Beaucoup d'accès aux données
- Pas de chat audio+texte
- pas de visio conf

#### Riot/matix :

- Gestion des droits ne nous convient pas
- Protocol lent
- Fédération pas forcément utile
- Pas de recherche de messages

#### Slack : 

- Limite du nombre de messages
- Recherde de message bof
- Pas de visio/stream

#### Teams : 

- Microsoft

</details>

## Fonctionnaliés 

 - [ ] Chat texte
 - [ ] Chat audio
 - [ ] Communautés (genre serveur Discord)
 - [ ] Client se connectant à plusieurs serveurs (à définir ce qu'il y a sur les seurveurs)
 - [ ] Visio conférence
 - [ ] Stream (1 vidéo vers plusieurs personnes)
 - [ ] Rôles au sein d'une communauté 
 - [ ] Avatar
 - [ ] Envoi fichiers
 - [ ] Support markdown
 - [ ] Recherche de texte dans les messages
 - [ ] Random lien de chat
 
 ## Contraintes 
 
 - Être le plus économe en utilisation serveur
 - Être économe en bande passante
 - Facile d'utilisation
 - Beau
 - Modulable (on doit pouvoir changer de protocol de chat/visio sans devoir tout recoder)
 
 
 \+ Ajoutez si j'en oubli
 
 ## Contributeurs 
 
 [@al26p](https://gitlab.com/al26p)

 [@toornax](https://gitlab.com/toornax)